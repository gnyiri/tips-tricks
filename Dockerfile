FROM ubuntu

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get -y install vim-nox build-essential git cmake cmake-curses-gui time astyle nano gdb clang-tidy
RUN git config --global http.sslverify false

WORKDIR /root
CMD [ "/bin/bash" ]