# tips &amp; tricks

## Create Docker image

docker build --tag tips-n-tricks-image .

## Launch Docker container

docker run -it --rm -v $(pwd):/prj-home tips-n-tricks-image
