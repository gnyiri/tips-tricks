find_program(CXX_CLANG_TIDY_EXE
  NAMES "clang-tidy-6.0"
  DOC "Path to clang-tidy executable"
)

if(NOT CXX_CLANG_TIDY_EXE)
  message(STATUS "clang-tidy not found.")
else()
  message(STATUS "clang-tidy found: ${CXX_CLANG_TIDY_EXE}")
  set(DO_CLANG_TIDY "${CXX_CLANG_TIDY_EXE}"
    "-checks=google-build-explicit-make-pair,\
    google-build-namespaces,\
    google-build-using-namespace,\
    google-default-arguments,\
    google-explicit-constructor,\
    google-global-names-in-headers,\
    google-readability-*,\
    google-runtime-int,\
    google-runtime-operator,\
    bugprone-assert-side-effect,\
    bugprone-bool-pointer-implicit-conversion,\
    bugprone-copy-constructor-init,\
    bugprone-integer-division,\
    bugprone-parent-virtual-call,\
    bugprone-sizeof-container,\
    bugprone-string-constructor,\
    bugprone-string-integer-assignment,\
    bugprone-string-literal-with-embedded-nul,\
    bugprone-suspicious-enum-usage,\
    bugprone-suspicious-semicolon,\
    bugprone-undefined-memory-manipulation,\
    bugprone-unused-raii,\
    bugprone-unused-return-value,\
    cppcoreguidelines-narrowing-conversions,\
    cppcoreguidelines-no-malloc,\
    cppcoreguidelines-pro-bounds-constant-array-index,\
    cppcoreguidelines-pro-type-cstyle-cast,\
    cppcoreguidelines-pro-type-member-init,\
    cppcoreguidelines-pro-type-static-cast-downcast,\
    cppcoreguidelines-special-member-functions,\
    llvm-*,\
    misc-definitions-in-headers,\
    misc-redundant-expression,\
    misc-static-assert,\
    misc-unconventional-assign-operator,\
    misc-uniqueptr-reset-release,\
    modernize-deprecated-headers,\
    modernize-loop-convert,\
    modernize-pass-by-value,\
    modernize-replace-auto-ptr,\
    modernize-use-auto,\
    modernize-use-bool-literals,\
    modernize-use-default-member-init,\
    modernize-use-emplace,\
    modernize-use-equals-default,\
    modernize-use-equals-delete,\
    modernize-use-nullptr,\
    modernize-use-override,\
    performance-*,\
    readability-*")
endif()
